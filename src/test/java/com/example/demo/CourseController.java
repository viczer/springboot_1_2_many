package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.models.Course;
import com.example.demo.models.Instructor;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.InstructorRepository;
@RunWith(SpringRunner.class)
@DataJpaTest
class CourseController {
	@Autowired
	private CourseRepository courseRepository;
	@Autowired
	private TestEntityManager entityManager;


	@Test
	void testGetCoursesByInstructor() {
		Instructor instructor1 = new Instructor("admin", "admin", "a@a.fr");
		Course c1 = new Course("Java", instructor1);
		Course courseSavedInDb = entityManager.persist(c1);
		List<Course> courseFromDb = courseRepository.findByInstructorId(instructor1.getId());
		assertThat(courseFromDb.contains(courseSavedInDb));
	}

	@Test
	void testSave() {
		Course c1 = new Course("admin");
		Course courseSavedInDb = entityManager.persist(c1);
		Course courseFromDb = courseRepository.getOne(courseSavedInDb.getId());
		assertEquals(courseSavedInDb, courseFromDb);
		assertThat(courseFromDb.equals(courseSavedInDb));
	}

	@Test
	void testUpdateCourse() {
		Course p1 = new Course("admin");
		Course pSavInDb = entityManager.persist(p1);
		Course pFromDb = courseRepository.getOne(pSavInDb.getId());
		pFromDb.setTitle("Tiodio");
		entityManager.persist(pFromDb);
		pFromDb = courseRepository.getOne(pSavInDb.getId());
		assertEquals(pFromDb.getTitle(),"Tiodio");
	}

	@Test
	void testDeleteCourse() {
		Course c1 = new Course();
		Course c2 = new Course();
		Course persist = entityManager.persist(c1);
		entityManager.persist(c2);

		entityManager.remove(persist);
		List<Course> AllCourseFromDb = courseRepository.findAll();
		List<Course> courseList = new ArrayList<>();

		for (Course users : AllCourseFromDb) {
			courseList.add(users);
		}
		assertThat(courseList.size()).isEqualTo(1);
	}

}
