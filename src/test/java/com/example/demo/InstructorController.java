package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;


import com.example.demo.models.Instructor;

import com.example.demo.repository.InstructorRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
class InstructorController {
	@Autowired
	private InstructorRepository userRepository;
	@Autowired
	private TestEntityManager entityManager;

	@Test
	void testGetInstructors() {
		Instructor p1 = new Instructor();
		Instructor p2 = new Instructor();
		entityManager.persist(p1);
		entityManager.persist(p2);
		List<Instructor> AllInstructorFromDb = userRepository.findAll();

		List<Instructor> usersList = new ArrayList<Instructor>();

		for (Instructor personne : AllInstructorFromDb) {
			usersList.add(personne);
		}
		assertThat(usersList.size()).isEqualTo(2);

	}

	@Test
	void testGetInstructorById() {
		Instructor p1 = new Instructor("admin", "admin", "11/06/1971");
		Instructor usersSavedInDb = entityManager.persist(p1);
		Instructor usersFromDb = userRepository.getOne(usersSavedInDb.getId());
		assertEquals(usersSavedInDb, usersFromDb);
		assertThat(usersFromDb.equals(usersSavedInDb));
	}

	@Test
	void testCreateUser() {
		Instructor p1 = new Instructor("admin", "admin", "11/06/1971");
		Instructor usersSavedInDb = entityManager.persist(p1);
		Instructor usersFromDb = userRepository.getOne(usersSavedInDb.getId());
		assertEquals(usersSavedInDb, usersFromDb);
		assertThat(usersFromDb.equals(usersSavedInDb));
	}

	@Test
	void testUpdateUser() {
		Instructor p1 = new Instructor("admin", "admin", "11/06/1971");
		Instructor pSavInDb = entityManager.persist(p1);
		Instructor pFromDb = userRepository.getOne(pSavInDb.getId());
		pFromDb.setFirstName("Tiodio");
		entityManager.persist(pFromDb);
		pFromDb = userRepository.getOne(pSavInDb.getId());
		assertEquals(pFromDb.getFirstName(),"Tiodio");
	}

	@Test
	void testDeleteUser() {
		Instructor p1 = new Instructor();
		Instructor p2 = new Instructor();
		Instructor persist = entityManager.persist(p1);
		entityManager.persist(p2);

		entityManager.remove(persist);
		List<Instructor> AllInstructorFromDb = userRepository.findAll();
		List<Instructor> userList = new ArrayList<>();

		for (Instructor users : AllInstructorFromDb) {
			userList.add(users);
		}
		assertThat(userList.size()).isEqualTo(1);
	}

}
