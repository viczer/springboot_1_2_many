package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
@EnableJpaAuditing //per autogenerazione
@SpringBootApplication
public class SpringBoot12ManyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot12ManyApplication.class, args);
	}

}
